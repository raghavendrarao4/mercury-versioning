# Version Routing and Circuit Breaker

## Version Routing
Version routing is a feature that supports service consumer to route events across multiple versions of the service provider. A given service consumer can choose to configure event requests to route to selected service provider service version(s). Micro-services architecture logically classifies services into different groups - Presentation micro-services (a.k.a backend-for-frontend or bff), business logic micro-services and edge / resource / data micro-services (Sam Newman's article reference https://samnewman.io/patterns/).

### Why Version Routing
Here a few reasons why version routing is needed:
 - Version routing for downstream services similar to API gateway controller supporting version routing for bff services
 - Support multiple service versions to coexist, smoothly transition new enhancements & features to end-users
 - Blue-green deployments, accurately configure routing for disaster recovery instances
 - Load balance requests from bff service consumers across multiple downstream service versions

### Version Routing feature in Mercury
Version routing is now introduced as a feature configurable in the platform Mercury.
With version routing:
 - Route names are externalize & configurable
 - Routing rules are configurable
 - Caller is not necessarily `version-aware`, routing has the rules configured already

## Circuit Breaker
Mercury platform-core exposes circuit breaker as a fall-back mechanism on an event of routing failure to version routes. Fall-back configuration is comprised of:
	- retry: configure circuit breaker for number of retries using fall-back route
	- hold: time to hold circuit breaker to avoid network overloading with retries

Note that for RPC call, retry would mean `3 x hold` value. Once the circuit breaker is opened, it will hold for a certain period. During an outage, alternate route will be executed right away without delay. If the alternate route fails, the exception will throw back to the caller.
 
There is an additional complexity for asynchronous calls, which needs a feedback circuit to tell if the target service runs without exception. This will be an additional enhancement to the current version, to also include response correlation.

Fallback & circuit breaker for async & RPC calls are design in-progress.

## Design approach
Version Routing applies ratio & proportions algorithm for load balancing. Configured event split ratio for the set of version routes is used to compute the `greatest common divisor (GCD)` also known as `highest common divisor (HCD)` or `highest common factor (HCF)`.
Event proportions for a given route is determined by dividing the route proportion by GCD. An example: 

```
Assume 4 service versions for service-A v1, v2, v3 & v4 and 
configured event split 2, 10, 28, 50 respectively.
GCD of 2, 10, 28, 50 is 2.
Ratio proportion = 2/2 : 10/2 : 28/2 : 50 / 2 = 1:5:14:25.

Assume 2 service versions for service-B v1, v2 and configured event split 25, 75 respectively.
GCD of 25 & 75 is 25.
Ratio proportion = 25/25 : 25/75 = 1:3.
```
In the first example, event split to target routes are in the ratio 1:5:14:25 for v1, v2, v3 & v4 routes respectively.
In the second example, event split to target routes are in the ratio 1:3 for v1 & v2 routes respectively.

Service interceptor dynamically splits the incoming events by the configured ratio. Configured ratios are loaded in a local hash map and incoming event count is incremented for each route until configured ratio limit. In the above examples, `service-A` proportion is reset for every `45 events` and `service-B` proportion is reset for every `4 events`. Hence, events are auto-load balanced based on:
  - Number of service versions
  - Ratio split for each version

This configuration allows flexibility to configure load balancing based on business requirements and downstream-service versioning.

Fallback design applies hold & retry times. On a routing failure, `hold time` configuration avoids blocking network with too many events. Service interceptor further redirects events to the configured `fallback route` after the hold time. In case of failures routing to `fallback route name`, service interceptor holds until the next `hold time` interval. Once configured number of retries are exhausted and interceptor encounters routing failure for the `fallback route`, a runtime exception is thrown back to the caller.

## Version Routing & Circuit breaker Components
Version routing & circuit breaker logic involves the following components:
 - Service Interceptor lambda function `ServiceInterceptor` for version routing
 - Configure Version routes in YAML `routes.yaml` with load balance configuration
 - Configure circuit breaker logic in YAML `routes.yaml`

Routing rules & circuit breaker logic is all embedded in `routes.yaml`, simple to configure and easily maintainable.
Sample YAML file is packaged under `resources` directory.

### How to invoke Service interceptor
Services need to register & invoke the lambda function `ServiceInterceptor` in order to use version routing & circuit breaker logic. Here is an example:

```
//register service interceptor class for version routing
platform.register ("service.interceptor.route", new ServiceInterceptor("classpath:/routes.yaml"), n);

```
 - `service.interceptor.route` is the generic route name, where `version unaware` events are routed to
 - `routes.yaml` is the configured YAML file with version routing & circuit breaker logic
 - `n` number of concurrent workers, based on the business use case

### YAML file structure
Here is the structure of YAML file `routes.yaml`

```
specs: 
  service-a: 
    fallback: 
      hold: 5m # hold circuit to avoid overloading the network with retries
      retry: 3 # number of retries
      route: v1.service-a.route # fallback or alternate route
    routes: 
      - v1.service-a.route: 1 # v1 route & events split
      - v2.service-a.route: 3 # v2 route & events split
      - v3.service-a.route: 5 # v3 route & events split
  service-b: 
    fallback: 
      hold: 5s # hold circuit to avoid overloading the network with retries
      retry: 3 # number of retries
      route: v1.service-b.route # fallback route
    routes: 
      - v1.service-b.route: 50 # v1 route & events split
      - v2.service-b.route: 50 # v2 route & events split
```

 - Each provider service is identified by the service name `service-a`, `service-b`, etc.
 - Each service is configured with `fallback` & destination service `routes`
 - Each destination service `route` is configured with load balancing split
 - Load balancing split can be configured as a percentage value or numeric (ratio) value. Example shows `percentage` & `numeric` ratio splits
 - Service interceptor distributes incoming events `proportionately` by the configured ratio
 - `fallback` logic follows the configured hold & retries on the alternate route

### Version routing & circuit breaker trials

`platform-core` project includes sample `routes.yaml` file with the following configuration.
Edit `routes.yaml` with the routing configuration of your choice.

```
  service-a: 
    routes: 
      - v1.service-a.route: 1 # v1 route & events split
      - v2.service-a.route: 3 # v2 route & events split
      - v3.service-a.route: 5 # v3 route & events split
      - v4.service-a.route: 7 # v4 route, events split
      - v5.service-a.route: 9 # v5 route, events split
```

Also configure the fallback route, hold time & number of retries of your choice for each service.
Sample values are already included in `routes.yaml`

```
  service-a: 
    fallback: 
      hold: 5s # hold circuit to avoid overloading the network with retries
      retry: 3 # number of retries
      route: v1.service-a.route # fallback route
```

`rest-examples` project contains sample services `DemoServiceV1`, `DemoServiceV2`, `DemoServiceV3`, `DemoServiceV4` & `DemoServiceV5`. Registered the versioned routes and service interceptor in the `MainApp`:

```
        //register versioned routes
        platform.register("v1.service-a.route", new DemoServiceV1(), n);
        platform.register("v2.service-a.route", new DemoServiceV2(), n);

        //register service interceptor class for service versioning
        platform.register("service.interceptor.route", 
        			new ServiceInterceptor("classpath:/routes.yaml"), n);
```

`rest-examples` project contains a sample REST service `JaxServiceInterceptor`, exposes 'GET' service endpoint `\api\hello\interceptor`. Query parameter `count` specifies the number of events to pump into the service interceptor.

Configure the `targetServiceName` in `JaxServiceInterceptor` to the target route of your choice. `targetServiceName` should match the service name configured in `routes.yaml`. This is the target service where the events get routed to.

```
	        response = po.request("service.interceptor.route", 3000, forward, 
	        							new Kv("targetServiceName", "service-a"));
```


With the circuit breaker & version route configuration, start the `event node` and spring boot app `rest-example` locally. Further, invoke the REST endpoint `http://localhost:8083/api/hello/interceptor?count=150` to see the results as below:

```
{
  "headers": {},
  "round_trip": 95.3499998152256,
  "body": " V1: 10   V2: 30   V3: 50   V4: 70   V5: 90   totalRequests: 250",
  "status": 200,
  "execution_time": 54.71500006318092
}

```
Event split by ratio for each target route, total number of events processed and execution round trip are displayed.


| Chapter-5                                 | Home                                     |
| :----------------------------------------:|:----------------------------------------:|
| [Versioning and Circuit Breaker](CHAPTER-4.md)        | [Table of Contents](TABLE-OF-CONTENTS.md)|