/*

    Copyright 2018-2019 Accenture Technology

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */

package org.platformlambda.core.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.platformlambda.core.annotations.EventInterceptor;
import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.models.Kv;
import org.platformlambda.core.models.LambdaFunction;
import org.platformlambda.core.system.PostOffice;
import org.platformlambda.core.util.ConfigReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service Interceptor Lambda function Impl 
 * Event Flow: 
 * 	Event Producer :: Invokes Service Interceptor 
 * 	Service Interceptor :: Route to Target
 * 	Destination Route(s) YAML config file :: read target route versions :: read
 * 	load balance config :: read fallback config
 */
@EventInterceptor
public class ServiceInterceptor implements LambdaFunction {

	private static final Logger log = LoggerFactory.getLogger(ServiceInterceptor.class); // Logger
	private ConfigReader configReader = new ConfigReader(); // Configuration reader

	private Kv[] eventHeaders; // Event headers
	private Object eventBody; // Event Body

	private Map<String, Object> configValues; // Configuration values from routes YAML file
	private Map<String, Object> loadBalanceMap; // Map with route names & load balance across routes
	private Map<String, Object> configuredRoutes; // List of destination routes with configured ratio
	private Map<String, Object> proportionRoutes; // List of destination routes with current proportion
	private Map<String, Object> fallback; // Fallback specifications

	/**
	 * Constructor
	 * 
	 * @param yamlFile
	 *            route service config file Loads routes YAML file
	 */
	public ServiceInterceptor(String yamlFile) {
		try {

			// Load YAML file with service versions configured
			configReader.load(yamlFile);
			// Flatten config values read
			configReader.flattenMap();

			configValues = configReader.getMap();

		} catch (Exception e) {
			log.error("Unable to load {}, {}", yamlFile, e.getMessage());
		}
	}

	/**
	 * Mercury handleEvent method
	 */
	@Override
	public Object handleEvent(Map<String, String> headers, Object body, int instance) throws Exception {

		// Set EventEnvelope body
		this.eventBody = body;
		// Event envelope response
		EventEnvelope response = null;

		// Set EventEnvelope header
		int counter = 0;
		eventHeaders = new Kv[headers.size()];
		for (String headerKey : headers.keySet()) {
			eventHeaders[counter] = new Kv(headerKey, headers.get(headerKey));
			counter++;
		}

		// Get service name from EventEnvelope header
		String serviceName = headers.get("targetServiceName");

		// Retrieve the service version routes if null or empty
		if (this.configuredRoutes == null || this.configuredRoutes.isEmpty()) {
			this.configuredRoutes = getRouteList(serviceName);
			
			//Reset load balance map
			if(this.loadBalanceMap == null)
				this.loadBalanceMap = new HashMap<>();
			else
				this.loadBalanceMap.clear();

			if(this.configuredRoutes != null && !configuredRoutes.isEmpty()) {
				// Populate load balance map
				loadBalanceMap.putAll(this.configuredRoutes);

				int[] ratio = new int[loadBalanceMap.size()];
				int ratioCtr=0;
				
				//Retrieve current load balance configuration
				for(Object value: loadBalanceMap.values()) {
					ratio[ratioCtr] = (int) value;
					ratioCtr++;
				}

				//Compute GCD of all routing destinations
				int greatestDivisor = findGCD(ratio);
				
				// Determine ratio proportion for each route
				// from GCD
				for (String key : loadBalanceMap.keySet()) {
					int value = (int) loadBalanceMap.get(key);
					int proportion = (value / greatestDivisor);
					loadBalanceMap.put(key, proportion);
				}
			}
		}

		// Retrieve fallback route details if null or empty
		if (this.fallback == null || this.fallback.isEmpty()) {
			this.fallback = getFallbackDetails(serviceName);
		}
		
		// Load balance requests proportionately by split ratio
		response = routeToTargets();

		return response.getBody();
	}

	/**
	 * Get Route List
	 * 
	 * @param serviceName
	 *            service name
	 * @return list of routes
	 */
	private Map<String, Object> getRouteList(String serviceName) {

		Map<String, Object> routesDtls = new HashMap<>();
		// Read:
		// - primary & secondary / route versions
		for (String key : configValues.keySet()) {

			if (key.contains(serviceName) && key.contains("routes")) {
				int value = (int) configValues.get(key);

				// Active routes have a positive value for route
				// Zero or negative values are ignored to route
				if (value > 0) {
					String routeName = key.substring((key.indexOf("].")) + 2, key.length());
					routesDtls.put(routeName, value);
				}
			}
		}
		
		return routesDtls;
	}

	/**
	 * Get Fallback details
	 * 
	 * @param serviceName
	 *            service name
	 * @return fallback details
	 */
	private Map<String, Object> getFallbackDetails(String serviceName) {

		Map<String, Object> fallbackDtls = new HashMap<>();

		// Read fall back configuration
		// - fall back route
		// - hold time
		// - number of retries
		for (String key : configValues.keySet()) {
			if (key.contains(serviceName) && key.contains("fallback")) {
				Object value = configValues.get(key);
				String property = key.substring((key.indexOf("fallback")) + 9, key.length());
				fallbackDtls.put(property, value);
			}
		}

		if (!fallbackDtls.isEmpty()) {

			int startIndex = 0;// start index
			int waitSec = 0; // wait duration
			String hold = (String) fallbackDtls.get("hold"); // hold time before fallback trigger

			// Parse wait time in minutes / seconds
			if (hold.contains("m")) {
				startIndex = hold.indexOf("m");
				waitSec = (Integer.parseInt(hold.substring(startIndex - 1, startIndex))) * 60;
			} else if (hold.contains("s")) {
				startIndex = hold.indexOf("s");
				waitSec = Integer.parseInt(hold.substring(startIndex - 1, startIndex));
			}

			fallbackDtls.put("hold", waitSec); // update wait time
		}

		return fallbackDtls;
	}

	/**
	 * Route To Targets
	 * 
	 * @return Event Envelope
	 */
	private EventEnvelope routeToTargets() throws Exception {

		PostOffice po = PostOffice.getInstance();
		EventEnvelope response = null;
		try {
			
			if(this.configuredRoutes == null || this.configuredRoutes.isEmpty()) {
				log.error("No configured versioned routes");
				// Fall back service route
				response = fallbackRoute();
			} else {

				// Compute Routing Proportion
				computeRoutingProportion();

				// Route to target destination
				// Apply load balancing with routing
				for (String key : this.proportionRoutes.keySet()) {
					int currentProportion = (int) proportionRoutes.get(key);
					int proportion = (int) loadBalanceMap.get(key);

					// Apply Load balancing with configured ratios and calculated proportions
					// Active routes are those with percentage split greater than zero
					if (currentProportion <= proportion) {

						response = po.request(key, 10000, this.eventBody, this.eventHeaders);
						currentProportion++;
						proportionRoutes.put(key, currentProportion);

						break; // route to one destination at a time
					}
				}
			}

		} catch (IOException | TimeoutException | AppException e) {
			log.error("FALL BACK SERVICE DUE TO {}", e.getMessage());
			// Fall back service route
			response = fallbackRoute();
		}
		return response;
	}

	/**
	 * Compute Routing Proportion across destination routes
	 */
	private void computeRoutingProportion() {

		if(this.proportionRoutes == null || this.proportionRoutes.isEmpty()) {
			this.proportionRoutes = new HashMap<>();
			this.proportionRoutes.putAll(this.configuredRoutes);
			
			//Reset values in the routes map
			for(String key: proportionRoutes.keySet()) {
				proportionRoutes.put(key, 1);
			}
		} else {
			// Check if current ratios for all routes
			// have reached the limited proportion
			int countLimit = 0;
			for (String key : proportionRoutes.keySet()) {
				int value = (int) proportionRoutes.get(key);
				int proportion = (int) loadBalanceMap.get(key);
				
				if(value > proportion)
					countLimit++;
				else
					break; //break even if a route hasn't yet reached proportioned ratio
			}
			
			if(countLimit == proportionRoutes.size()) {
				//Reset values in the routes map
				for(String key: proportionRoutes.keySet()) {
					proportionRoutes.put(key, 1);
				}
			}
		}
	}

	/**
	 * Function to find gcd of array of N numbers
	 * 
	 * @param arr
	 * @return GCD of N numbers
	 */
	private int findGCD(int arr[]) {
		int result = arr[0];
		int n = arr.length;
		for (int i = 1; i < n; i++)
			result = gcd(arr[i], result);

		return result;
	}

	/**
	 * Recursive function to return gcd of two numbers a and b
	 * 
	 * @param a
	 * @param b
	 * @return GCD (greatest common divisor)
	 */
	private int gcd(int a, int b) {
		if (b == 0)
			return a;
		return gcd(b, a % b);
	}

	/**
	 * Fallback route
	 * 
	 * @return Event envelope
	 */
	private EventEnvelope fallbackRoute() throws Exception {

		PostOffice po = PostOffice.getInstance();
		EventEnvelope responseDtls = null;
		int waitSec = 0; // wait duration
		int retry = 0; // retry count

		try {

			waitSec = (int) this.fallback.get("hold"); // hold time before fallback trigger
			retry = (int) this.fallback.get("retry"); // retry count
			String fallbackRoute = (String) this.fallback.get("route"); // fallback route

			// Hold time for fallback trigger
			Thread.sleep(waitSec * 1000);

			responseDtls = po.request(fallbackRoute, 10000, this.eventBody, this.eventHeaders);

		} catch (IOException | TimeoutException | AppException exception) {
			log.error("FALLBACK ROUTE {} FAILED ", exception.getMessage());
			// Decrement retry count
			retry -= 1;

			if (retry >= 1) {
				// Retry fallback route on a failure
				this.fallback.put("retry", retry);
				responseDtls = fallbackRoute();
			} else {
				// Throw an exception after retry count is exhausted
				throw new RuntimeException(exception);
			}
		}
		return responseDtls;
	}
}