package com.accenture.examples.rest;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.models.Kv;
import org.platformlambda.core.system.PostOffice;

/**
 * Service Interceptor REST example
 * Used to invoke Service Interceptor class to determine
 * the target route for versioned services
 *
 */
@Path("/hello")
public class JaxServiceInterceptor {

	private static AtomicInteger seq = new AtomicInteger(0);
	
    @GET
    @Path("/interceptor")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_HTML})
    public Map<String, Object> interceptor (@Context HttpServletRequest request, @QueryParam("count") Long count) throws IOException, TimeoutException, AppException {
    	
        PostOffice po = PostOffice.getInstance();

        Map<String, Object> forward = new HashMap<>();
        forward.put("time", new Date());

        Enumeration<String> headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String key = headers.nextElement();
            forward.put(key, request.getHeader(key));
        }

        EventEnvelope response = null;
        Map<String, String> respSplit = new HashMap<>();
        
        if(count == null || count == 0)
        	count = 100L;

        double executionTime = 0.0;
        double roundTripTime = 0;
        
        for(long i=0; i<count; i++) {
        	
	        //Include sequence number and target service interceptor route name
	        // in the request Kv (header)
	        response = po.request("service.interceptor.route", 3000, forward, 
	        							new Kv("targetServiceName", "service-a"));
	        
	        String resp = (String) response.getBody();
	        String key = resp.substring(0,2);
	        String value = resp.substring(3);
	        respSplit.put(key, value);
	        executionTime = executionTime + response.getExecutionTime();
	        roundTripTime = roundTripTime + response.getRoundTrip();
        }

        String resultStr = " ";
        long total=0;
        for(Object key: respSplit.keySet()) {
        	long value = Long.parseLong(respSplit.get(key));
       		resultStr = resultStr + key + ": " + value + "   ";
       		total = total+value;
        }
        resultStr = resultStr + "totalRequests: " + total;
        
        Map<String, Object> result = new HashMap<>();
        result.put("status", response.getStatus());
        result.put("headers", response.getHeaders());
        result.put("body", resultStr);
        result.put("execution_time", executionTime);
        result.put("round_trip", roundTripTime);

        return result;
    }
}
